﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ApplicationSimpleDemo")> 
<Assembly: AssemblyDescription("Arenal Client Demo App")> 
<Assembly: AssemblyCompany("SKYWARE Group")> 
<Assembly: AssemblyProduct("ApplicationSimpleDemo")>
<Assembly: AssemblyCopyright("Copyright © SKYWARE Group 2015-2021")>
<Assembly: AssemblyTrademark("SKYWARE Group")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("10e4df67-5dfd-4a1b-9048-4aed0a27a87a")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
