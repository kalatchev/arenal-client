﻿
Imports Skyware.Arenal

Module ModuleMain

    'В зависимост коя демо функция ползвате, трябва да въведете реални данни.

    'Private Const SRV_BASE_URL As String = "http://localhost:8080/Arenal/api"
    Private Const SRV_BASE_URL As String = "http://arenal.skyware-group.com:8080/Arenal/api"
    Private Const RES_BASE_URL As String = "http://localhost:8080/iLabAres/api"
    Private Const VENDOR_ID As Integer = 0 'Въведете реални данни
    Private Const VENDOR_TOKEN As String = "**********" 'Въведете реални данни
    Private Const CONSUMER_TOKEN_ID As Integer = 0 'Въведете реални данни
    Private Const CONSUMER_TOKEN As String = "**********" 'Въведете реални данни


    Sub Main()
        PublicDataDemo()
        'PublisherActionsDemo(VENDOR_ID, VENDOR_TOKEN)
        'PublisherActionsDemoExt(VENDOR_ID, VENDOR_TOKEN)
        'ConsumerDemo(CONSUMER_TOKEN_ID, CONSUMER_TOKEN)
        'ConsumerDemoShort(CONSUMER_ID, CONSUMER_TOKEN)
        'CardProviderDemo() 'Внимание - виж кода за данни!
        'ResDemo() 'Внимание - виж кода за данни!
        'ResDemoExt() 'Внимание - виж кода за данни!
    End Sub

    Private Sub ConsumerDemoShort(ConsumerID As Integer, ConsumerToken As String)
        Dim Clt As New ArenalClient(SRV_BASE_URL, "")

        Console.Clear()

        'TODO: Въедете реален ID
        Dim CardID As Integer = 0
        Console.WriteLine($"Преглед на НМДД с ID {CardID}:")
        Dim Card As Card
        Try
            Card = Clt.GetCard(CardID, $"{ConsumerID}-{ConsumerToken}")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo?.Details)
            Console.Write("Натиснете клавиш за изход ...")
            Console.ReadKey()
            Exit Sub
        End Try

        Console.WriteLine($"НМДД с ID {Card.ArenalID}")
        Console.WriteLine($"Пациент: {Card.GivenName} {Card.MiddleName} {Card.FamilyName}")
        Console.WriteLine($"Пациент ID: {Card.PID}")
        'Console.WriteLine($"Пациент ID: {Card.PID}")

        Console.Write("Натиснете клавиш за изход ...")
        Console.ReadKey()
    End Sub

    Private Sub ConsumerDemo(ConsumerID As Integer, ConsumerToken As String)
        Dim Clt As New ArenalClient(SRV_BASE_URL, "")

        Console.Clear()

        Dim pid As String = "6410049111"
        Console.WriteLine($"Търсене на НМДД с ЕГН/ЕНЧ {pid}:")
        Dim CardsFound As Cards
        Try
            CardsFound = Clt.SearchCards(pid, $"{ConsumerID}-{ConsumerToken}")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo.Details)
            Console.Write("Натиснете клавиш за изход ...")
            Console.ReadKey()
            Exit Sub
        End Try
        For Each CurCard As Card In CardsFound.Items
            Console.WriteLine(
                $"{CurCard.ArenalID}: {CurCard.GivenName} {CurCard.FamilyName} НМДД {CurCard.CardNo}/{CurCard.Issued.ToShortDateString}, лекар {CurCard.PublisherID}")
        Next
        If CardsFound IsNot Nothing AndAlso CardsFound.Items.Count > 0 Then
            Dim TestCard As Card = CardsFound.Items.First
            Console.Write($"Взимане на НМДД {TestCard.ArenalID}: ")
            Try
                Clt.TakeCard(TestCard, $"{ConsumerID}-{ConsumerToken}")
                Console.WriteLine($"ОК (Result token: {TestCard.ResultsToken})")
            Catch ex As ArenalException
                Console.WriteLine("грешка.")
                Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
                Console.WriteLine(ex.ErrorInfo.Details)
                Console.Write("Натиснете клавиш за изход ...")
                Console.ReadKey()
                Exit Sub
            End Try

            Dim CardPublisher As Publisher
            Console.Write($"Взимане на издател {TestCard.PublisherID}: ")
            Try
                CardPublisher = Clt.GetPublisher(TestCard.PublisherID, $"{ConsumerID}-{ConsumerToken}")
                Console.WriteLine($"ОК ({CardPublisher.GivenName} {CardPublisher.FamilyName} {CardPublisher.UIN})")
            Catch ex As ArenalException
                Console.WriteLine("грешка.")
                Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
                Console.WriteLine(ex.ErrorInfo.Details)
            End Try

            Console.Write($"Освобождаване на НМДД {TestCard.ArenalID}: ")
            Try
                Clt.ReleaseCard(TestCard)
                Console.WriteLine("ОК")
            Catch ex As ArenalException
                Console.WriteLine("грешка.")
                Console.WriteLine(
                    String.Format("Грешка {0}: {1}", ex.ErrorInfo.ErrorCode, ex.ErrorInfo.Message))
                Console.WriteLine(ex.ErrorInfo.Details)
                Console.Write("Натиснете клавиш за изход ...")
                Console.ReadKey()
                Exit Sub
            End Try
        Else
            Console.WriteLine("Няма намерени свободни НМДД.")
        End If

        Console.Write("Натиснете клавиш за изход ...")
        Console.ReadKey()
    End Sub

    ''' <summary>
    ''' Демонстрира (само)регистрация на Издател и Направление. 
    ''' Издателя е титуляр, пациента е БГ гражданин с ЕГН.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PublisherActionsDemo(VendorID As Integer, VendorToken As String)
        Dim VendorCompositeToken = $"{VendorID}-{VendorToken}"
        'Създава клеинта
        Dim Clt As New ArenalClient(SRV_BASE_URL, VendorCompositeToken)

        Console.Clear()

        'Създава обект Publisher а данни за него
        Dim Doctor As New Publisher With {
            .UIN = "0200000088",
            .GivenName = "Александър", .MiddleName = "Благоев", .FamilyName = "Лазаров",
            .Speciality = "15",
            .PracticeCode = "2222134501", .PracticeName = "ДКЦ ХVI СОФИЯ ООД",
            .PracticeCity = "София", .PracticePostalCode = "1618", .PracticeAddress = "КВ. БОТУНЕЦ, УЛ.И. ШОНЕВ 2",
            .Email = "example@domain.com"}

        'Регистрира Publisher (обикновено се прави веднъж, при инсталация на ОПЛ софтуера)
        Try
            Console.Write("Регистрира лекар ... ")
            Clt.RegisterPublisher(Doctor, VendorCompositeToken)
            Console.WriteLine("ОК")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo.Details)
            Console.WriteLine("Натиснете клавиш за да продължите ...")
            Console.ReadKey()
            Exit Sub
        End Try


        'Тук трябва да вземете Doctor.ArenalID и Doctor.Token
        'и да ги запишете за бъдеща употреба

        'Създава НМДД с данни
        Dim MyCard As New Card With {
            .PublisherRole = 0,
            .CardNo = 15, .AmbulatoryNo = 45, .VisitType = 1,
            .PIDType = 1, .PID = "6101047500", .DOB = New Date(1973, 5, 5),
            .GivenName = "Иван", .MiddleName = "Петров", .FamilyName = "Маринов",
            .RHIF = "22", .HealthRegion = "01",
            .LeadingDiagnosis = "Z00.01"}
        MyCard.Examinations.Add(New Examination With {.Code = "01.01"})

        'Променя token на клиента, с token на издателя
        Clt.Token = $"{Doctor.ArenalID}-{Doctor.Token}"

        'Регистрира НМДД
        Try
            Console.Write("Регистрира НМДД ... ")
            Clt.RegisterCard(MyCard)
            Console.WriteLine("ОК")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo.Details)
            Console.WriteLine("Натиснете клавиш за да продължите ...")
            Console.ReadKey()
            Exit Sub
        End Try


        'Тук трябва да вземете MyCard.ArenalID и MyCard.Token
        'и да ги запишете за бъдеща употреба

        Console.Write("Натиснете клавиш за изход ...")
        Console.ReadKey()
    End Sub

    ''' <summary>
    ''' Демонстрира (само)регистрация на Издател и Направление. 
    ''' Издателя е нает към друг ОПЛ, пациента е френски гражданин.
    ''' </summary>
    ''' <param name="VendorID"></param>
    ''' <param name="VendorToken"></param>
    ''' <remarks></remarks>
    Private Sub PublisherActionsDemoExt(VendorID As Integer, VendorToken As String)
        Dim VendorCompositeToken = $"{VendorID}-{VendorToken}"
        'Създава клеинта
        Dim Clt As New ArenalClient(SRV_BASE_URL, VendorCompositeToken)

        Console.Clear()

        'Създава обект Publisher а данни за него
        Dim Doctor As New Publisher With {
            .UIN = "0200000088",
            .GivenName = "Александър", .MiddleName = "Благоев", .FamilyName = "Лазаров",
            .Speciality = "00",
            .PracticeCode = "1601111501", .PracticeName = "ОПЛ ООД",
            .PracticeCity = "София", .PracticePostalCode = "1618", .PracticeAddress = "КВ. БОТУНЕЦ, УЛ.И. ШОНЕВ 2",
            .Email = "example@domain.com"}

        'Регистрира Publisher (обикновено се прави веднъж, при инсталация на ОПЛ софтуера)
        Try
            Console.Write("Регистрира лекар ... ")
            Clt.RegisterPublisher(Doctor, VendorCompositeToken)
            Console.WriteLine("ОК")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo.Details)
            Console.WriteLine("Натиснете клавиш за да продължите ...")
            Console.ReadKey()
            Exit Sub
        End Try


        'Тук трябва да вземете Doctor.ArenalID и Doctor.Token
        'и да ги запишете за бъдеща употреба

        'Създава обект титуляр на практиката
        Dim Holder As New PracticeHolder With {
            .UIN = "0500000134",
            .GivenName = "Борислав", .MiddleName = "Иванов", .FamilyName = "Николов",
            .Speciality = "00",
            .PracticeCode = "1601111501", .PracticeName = "ОПЛ ООД"}

        'Създава НМДД с данни
        Dim MyCard As New Card With {
            .PublisherRole = 2,
            .PracticeHolder = Holder,
            .CardNo = 88, .AmbulatoryNo = 125, .VisitType = 1,
            .CountryCode = "FR", .PID = "1234/12.23.56-5", .DOB = New Date(1973, 5, 5),
            .GivenName = "Мари", .FamilyName = "Жирард",
            .RHIF = "22", .HealthRegion = "01",
            .LeadingDiagnosis = "Z00.01"}
        MyCard.Examinations.Add(New Examination With {.Code = "01.11"})

        'Променя token на клиента, с token на издателя
        Clt.Token = $"{Doctor.ArenalID}-{Doctor.Token}"

        'Регистрира НМДД
        Try
            Console.Write("Регистрира НМДД ... ")
            Clt.RegisterCard(MyCard)
            Console.WriteLine("ОК")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo.Details)
            Console.WriteLine("Натиснете клавиш за да продължите ...")
            Console.ReadKey()
            Exit Sub
        End Try

        MyCard.CardNo += 1

        'Обновява НМДД
        Try
            Console.Write("Обновява НМДД ... ")
            Clt.UpdateCard(MyCard)
            Console.WriteLine("ОК")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo.Details)
            Console.WriteLine("Натиснете клавиш за да продължите ...")
            Console.ReadKey()
            Exit Sub
        End Try

        'Анулира(изтрива) НМДД
        Try
            Console.Write("Анулира НМДД ... ")
            Clt.DeleteCard(MyCard.ArenalID, MyCard.Token)
            Console.WriteLine("ОК")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo.Details)
            Console.WriteLine("Натиснете клавиш за да продължите ...")
            Console.ReadKey()
            Exit Sub
        End Try


        'Тук трябва да вземете MyCard.ArenalID и MyCard.Token
        'и да ги запишете за бъдеща употреба

        Console.Write("Натиснете клавиш за изход ...")
        Console.ReadKey()
    End Sub

    ''' <summary>
    ''' Демонстрира извличане на публични данни (номенклатури)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PublicDataDemo()

        Dim Clt As New ArenalClient(SRV_BASE_URL, "")

        'Извличане на РЗОК, области, общини и здравни региони
        Console.Clear()
        Console.WriteLine("РЗОК и Здравни региони -----------------")
        Console.WriteLine()
        Dim Regions As Regions = Clt.GetRegions
        For Each CurRegion As Region In Regions.Items
            Console.WriteLine($"{ CurRegion.Code} {CurRegion.Name}")
            For Each CurMun As Municipality In CurRegion.Municipalities
                Console.WriteLine($"  {CurMun.HRCode} {CurMun.Name}")
            Next
        Next
        Console.WriteLine()
        Console.WriteLine("Натиснете клавиш за да продължите ...")
        Console.ReadKey()

        'Извличане на типове пациентски идентификатори
        Console.Clear()
        Console.WriteLine("Типове идентификатори ------------------")
        Console.WriteLine()
        Dim PIDTypes As PIDTypes = Clt.GetPIDTypes
        For Each CurType As PIDType In PIDTypes.Items
            Console.WriteLine($"{CurType.ID} {CurType.Name}")
        Next
        Console.WriteLine()
        Console.WriteLine("Натиснете клавиш за да продължите ...")
        Console.ReadKey()

        'Извличане на роли на поръчващия лекар
        Console.Clear()
        Console.WriteLine("Роли на поръчващия лекар -------------")
        Console.WriteLine()
        Dim OrdTypes As PublisherRoles = Clt.GetPublisherRoles
        For Each CurType As PublisherRole In OrdTypes.Items
            Console.WriteLine($"{CurType.ID} {CurType.Name}")
        Next
        Console.WriteLine()
        Console.WriteLine("Натиснете клавиш за да продължите ...")
        Console.ReadKey()

        'Извлича видове посещения
        Console.Clear()
        Console.WriteLine("Видове посещения -------------------------")
        Console.WriteLine()
        Dim VisitTypes As VisitTypes = Clt.GetVisitTypes
        For Each CurType As VisitType In VisitTypes.Items
            Console.WriteLine($"{CurType.ID} {CurType.Name}")
        Next
        Console.WriteLine()
        Console.WriteLine("Натиснете клавиш за да продължите ...")
        Console.ReadKey()

        'Извлича пакети и излседвания
        Console.Clear()
        Console.WriteLine("Пакети и изследвания ---------------------")
        Console.WriteLine()
        Dim NHIFPacks As NHIFPacks = Clt.GetPacks
        For Each CurPack As NHIFPack In NHIFPacks.Items
            Console.WriteLine($"{CurPack.Code} {CurPack.Name}")
            For Each CurExam As NHIFExam In CurPack.NHIFExams
                Console.WriteLine($"  {CurExam.Code} {CurExam.Name}")
            Next
        Next
        Console.WriteLine()
        Console.WriteLine("Натиснете клавиш за да продължите ...")
        Console.ReadKey()

        '---Practice types?

        'Извлича специалности
        Console.Clear()
        Console.WriteLine("Специалности -----------------------------")
        Console.WriteLine()
        Dim Specs As Specialities = Clt.GetSpecialities
        For Each CurSpec As Speciality In Specs.Items
            Console.WriteLine($"{CurSpec.Code} {CurSpec.Name}")
        Next
        Console.WriteLine()
        Console.WriteLine("Натиснете клавиш за да продължите ...")
        Console.ReadKey()

        'Извлича държави
        Console.Clear()
        Console.WriteLine("Държави -----------------------")
        Console.WriteLine()
        Dim Cntrs As Countries = Clt.GetCountries
        For Each CurCountry As Country In Cntrs.Items
            Console.WriteLine($"{CurCountry.Code} {CurCountry.Name}")
        Next
        Console.WriteLine()

        'Извлича изпълнители
        Console.Clear()
        Console.WriteLine("Изпълнители -----------------------")
        Console.WriteLine()
        Dim Prvs As ServiceProviders = Clt.GetProviders
        For Each Prv As ServiceProvider In Prvs.Items
            Console.WriteLine($"{Prv.Name} - {Prv.City}")
        Next
        Console.WriteLine()

        Console.Write("Натиснете клавиш за изход ...")
        Console.ReadKey()
    End Sub

    Private Sub CardProviderDemo()
        Dim Clt As New ArenalClient(SRV_BASE_URL, "")

        Console.Clear()

        Dim CardID As Integer = 0
        Console.WriteLine($"Извличане на консуматор на НМДД {CardID}:")
        Dim Prv As ServiceProvider
        Try
            'TODO: Въведете реален ID и токен за да тествате
            Prv = Clt.GetCardProvider(CardID, "**********")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo?.Details)
            Console.Write("Натиснете клавиш за изход ...")
            Console.ReadKey()
            Exit Sub
        End Try

        Console.WriteLine($"Изпълнител на НМДД {CardID}: {Prv.Name} - {Prv.City}")
        Console.WriteLine($"Base Ares url: {Prv.PrimaryAres}")

        Console.Write("Натиснете клавиш за изход ...")
        Console.ReadKey()
    End Sub

    Private Sub ResDemo()
        Dim Clt As New ArenalClient(RES_BASE_URL, "")

        Console.Clear()

        'TODO: Въедете реален ID
        Dim CardID As Integer = 0
        Console.WriteLine($"Преглед на Резултати с ID {CardID}:")
        Dim Card As Card
        Try
            'TODO: Въедете реален токен за резултат
            Card = Clt.GetCardResultsDirect(RES_BASE_URL, CardID, "**********")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo?.Details)
            Console.Write("Натиснете клавиш за изход ...")
            Console.ReadKey()
            Exit Sub
        End Try

        Console.WriteLine($"НМДД с ID {Card.ArenalID}")
        Console.WriteLine($"Пациент: {Card.GivenName} {Card.MiddleName} {Card.FamilyName}")
        Console.WriteLine($"Пациент ID: {Card.PID}")
        Console.WriteLine($"Дата на визита: {Card.VisitDate}")
        Console.WriteLine($"Дата на изпълнение: {Card.Fulfilled}")
        Console.WriteLine($"Статус на НМДД: {Card.Status}")
        For Each CurExam As Examination In Card.Examinations
            Console.WriteLine("  " + CurExam.Code)
            For Each CurRes As Result In CurExam.Results
                Console.WriteLine("    " + $"{CurRes.Name}: {CurRes.TextResult} {CurRes.Units} [{CurRes.Range}] {CurRes.Flag} ({CurRes.Status})")
            Next
        Next

        Console.Write("Натиснете клавиш за изход ...")
        Console.ReadKey()
    End Sub

    Private Sub ResDemoExt()
        Dim Clt As New ArenalClient(SRV_BASE_URL, "")

        Console.Clear()

        Dim Card As Card
        Try
            'TODO: Въведете реален ID и токен за да тествате
            Card = Clt.GetCardResults(0, "**********")
        Catch ex As ArenalException
            Console.WriteLine("грешка.")
            Console.WriteLine($"Грешка {ex.ErrorInfo?.ErrorCode}: {ex.ErrorInfo?.Message}")
            Console.WriteLine(ex.ErrorInfo?.Details)
            Console.Write("Натиснете клавиш за изход ...")
            Console.ReadKey()
            Exit Sub
        End Try

        Console.WriteLine($"НМДД ID {Card.ArenalID}")
        Console.WriteLine($"Пациент: {Card.GivenName} {Card.MiddleName} {Card.FamilyName} ({Card.PID})")
        Console.WriteLine($"Изпълнител: Д-р {Card.ProviderDoctor.GivenName} {Card.ProviderDoctor.FamilyName} ({Card.ProviderDoctor.UIN})")
        Console.WriteLine($"Забележка: {Card.Note}")
        Console.WriteLine($"Статус на НМДД: {Card.Status}")
        For Each CurExam As Examination In Card.Examinations
            Console.WriteLine("  " + CurExam.Code)
            For Each CurRes As Result In CurExam.Results
                Console.WriteLine($"    {CurRes.LoincID}:{CurRes.Name} = {CurRes.TextResult} {CurRes.Units} [{CurRes.Range}] ({CurRes.Status})")
                If Not String.IsNullOrWhiteSpace(CurRes.Note) Then Console.WriteLine("    " + $"{CurRes.Note}")
            Next
        Next

        Console.Write("Натиснете клавиш за изход ...")
        Console.ReadKey()
    End Sub

End Module
