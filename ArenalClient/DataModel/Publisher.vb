﻿Imports System.Xml.Serialization
Imports System.Xml
Imports System.Text
Imports System.IO


<XmlType(TypeName:="Publisher")>
Public Class Publisher

    <XmlElement("ID")>
    Public Property ArenalID As Integer

    <XmlElement("UIN")>
    Public Property UIN As String

    <XmlElement("GivenName")>
    Public Property GivenName As String

    <XmlElement("MiddleName")>
    Public Property MiddleName As String

    <XmlElement("FamilyName")>
    Public Property FamilyName As String

    <XmlElement("Email")>
    Public Property Email As String

    <XmlElement("Speciality")>
    Public Property Speciality As String

    <XmlElement("PracticeCode")>
    Public Property PracticeCode As String

    <XmlElement("PracticeName")>
    Public Property PracticeName As String

    <XmlElement("PracticeCity")>
    Public Property PracticeCity As String

    <XmlElement("PracticePostalCode")>
    Public Property PracticePostalCode As String

    <XmlElement("PracticeAddress")>
    Public Property PracticeAddress As String

    <XmlElement("Token")>
    Public Property Token As String

    <XmlElement("IsValid")>
    Public Property IsValid As Boolean

End Class




