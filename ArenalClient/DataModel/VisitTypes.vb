﻿Imports System.Runtime.InteropServices
Imports System.Xml.Serialization

<XmlRoot("VisitTypes")>
Public Class VisitTypes

    <XmlElement("VisitType")>
    Public Property Items As List(Of VisitType) = New List(Of VisitType)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    ''' <returns>Array Of Objects</returns>
    Public Function ItemsAsArrayOfObjects() As <MarshalAs(UnmanagedType.Struct, SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Return Me.Items.Cast(Of Object).ToArray
    End Function

End Class

<XmlRoot("VisitType")>
Public Class VisitType

    <XmlElement("ID")>
    Public Property ID As Integer = 1

    <XmlElement("Name")>
    Public Property Name As String = ""

End Class


