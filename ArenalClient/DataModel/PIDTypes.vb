﻿Imports System.Runtime.InteropServices
Imports System.Xml.Serialization

<XmlRoot("PIDTypes")>
Public Class PIDTypes

    <XmlElement("PIDType")>
    Public Property Items As List(Of PIDType) = New List(Of PIDType)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    ''' <returns>Array Of Objects</returns>
    Public Function ItemsAsArrayOfObjects() As <MarshalAs(UnmanagedType.Struct, SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Return Me.Items.Cast(Of Object).ToArray
    End Function

End Class

<XmlRoot("PIDType")>
Public Class PIDType

    <XmlElement("ID")>
    Public Property ID As Integer = 0

    <XmlElement("Name")>
    Public Property Name As String = ""

End Class





