﻿Imports System.Runtime.InteropServices
Imports System.Xml.Serialization

<XmlRoot("Regions")>
Public Class Regions

    <XmlElement("Region")>
    Public Property Items As List(Of Region) = New List(Of Region)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    ''' <returns>Array Of Objects</returns>
    Public Function ItemsAsArrayOfObjects() As <MarshalAs(UnmanagedType.Struct, SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Return Me.Items.Cast(Of Object).ToArray
    End Function

End Class

<XmlRoot("Region")>
Public Class Region

    <XmlElement("Code")>
    Public Property Code As String

    <XmlElement("Name")>
    Public Property Name As String

    <XmlElement("BMACode")>
    Public Property BMACode As String

    <XmlElement("Municipality")>
    Public Property Municipalities As List(Of Municipality) = New List(Of Municipality)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    ''' <returns>Array Of Objects</returns>
    Public Function ItemsAsArrayOfObjects() As <MarshalAs(UnmanagedType.Struct, SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Return Me.Municipalities.Cast(Of Object).ToArray
    End Function

End Class

<XmlRoot("Municipality")>
Public Class Municipality

    <XmlElement("Code")>
    Public Property Code As String

    <XmlElement("HRCode")>
    Public Property HRCode As String

    <XmlElement("Name")>
    Public Property Name As String

End Class