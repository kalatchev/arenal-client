﻿Imports System.Runtime.InteropServices
Imports System.Xml.Serialization

<XmlRoot("ServiceProviders")>
Public Class ServiceProviders

    <XmlElement("ServiceProvider")>
    Public Property Items As List(Of ServiceProvider) = New List(Of ServiceProvider)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    ''' <returns>Array Of Objects</returns>
    Public Function ItemsAsArrayOfObjects() As <MarshalAs(UnmanagedType.Struct, SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Return Me.Items.Cast(Of Object).ToArray
    End Function

End Class

<XmlRoot("ServiceProvider")>
Public Class ServiceProvider

    <XmlElement("ID")>
    Public Property ArenalID As Integer

    <XmlElement("Name")>
    Public Property Name As String

    <XmlElement("City")>
    Public Property City As String

    <XmlElement("PostalCode")>
    Public Property PostalCode As String

    <XmlElement("Address")>
    Public Property Address As String

    <XmlElement("PracticeCode")>
    Public Property PracticeCode As String

    <XmlElement("PrimaryAres")>
    Public Property PrimaryAres As String

    <XmlElement("BackupAres")>
    Public Property BackupAres As String

    <XmlElement("WebSite")>
    Public Property WebSite As String

    <XmlElement("Phone1")>
    Public Property Phone1 As String

    <XmlElement("Phone2")>
    Public Property Phone2 As String

End Class
