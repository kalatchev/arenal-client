﻿Imports System.Net
Imports System.Xml.Serialization

Public Class ArenalException
    Inherits Exception

    Public Const ERROR_RECEIVING_DATA As String = "Грешка при получаване на данни от Аренал."
    Public Const NO_DATA_OR_WRONG_FORMAT As String = "Аренал не върна данни или техния формат е непознат."
    Public Const ARENAL_ERROR As String = "Аренал грешка."
    Public Const PAT_DOESNT_MATCH As String = "Пациентът на публикуваното и прочетено от Арес НММД не съвпадат."
    Public Const CARD_ISNT_TAKEN As String = "Опит за четене на резултати на НММД което не е взето."

    Public Sub New(Optional Message As String = "", Optional InnerException As Exception = Nothing)
        MyBase.New(Message, InnerException)
    End Sub

    Public Overloads Property Message As String

    Public Property ErrorInfo As ArenalError

End Class
