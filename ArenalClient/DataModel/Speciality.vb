﻿Imports System.Runtime.InteropServices
Imports System.Xml.Serialization

<XmlRoot("Specialities")>
Public Class Specialities

    <XmlElement("Speciality")>
    Public Property Items As List(Of Speciality) = New List(Of Speciality)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    ''' <returns>Array Of Objects</returns>
    Public Function ItemsAsArrayOfObjects() As <MarshalAs(UnmanagedType.Struct, SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Return Me.Items.Cast(Of Object).ToArray
    End Function

End Class

<XmlRoot("Speciality")>
Public Class Speciality

    <XmlElement("Code")>
    Public Property Code As String

    <XmlElement("Name")>
    Public Property Name As String

End Class