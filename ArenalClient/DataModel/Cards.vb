﻿Imports System.Runtime.InteropServices
Imports System.Xml.Serialization

<XmlRoot("Cards")>
Public Class Cards

    <XmlElement("Card")>
    Public Property Items As List(Of Card) = New List(Of Card)

End Class

<XmlRoot("Card")>
Public Class Card
    Implements ICloneable

    <XmlElement("ID")>
    Public Property ArenalID As Integer

    <XmlElement("PublisherID")>
    Public Property PublisherID As Integer

    <XmlElement("PublisherRole")>
    Public Property PublisherRole As Integer

    <XmlElement("PracticeHolder")>
    Public Property PracticeHolder As PracticeHolder = Nothing

    <XmlElement("Issued", DataType:="date")>
    Public Property Issued As Date

    <XmlElement("VisitDate", DataType:="date")>
    Public Property VisitDate As Date

    <XmlElement("Fulfilled", DataType:="date")>
    Public Property Fulfilled As Date

    <XmlElement("GivenName")>
    Public Property GivenName As String

    <XmlElement("MiddleName")>
    Public Property MiddleName As String = Nothing

    <XmlElement("FamilyName")>
    Public Property FamilyName As String

    <XmlElement("VisitType")>
    Public Property VisitType As Integer

    <XmlElement("PIDType")>
    Public Property PIDType As Integer? = Nothing

    ''' <summary>
    ''' COM helper (0=null)
    ''' </summary>
    <XmlIgnore()>
    Public Property NNPIDType As Integer
        Get
            If Me.PIDType.HasValue Then Return Me.PIDType.Value
            Return 0
        End Get
        Set(value As Integer)
            If value = 0 Then
                Me.PIDType = Nothing
            Else
                Me.PIDType = value
            End If
        End Set
    End Property

    Public Function ShouldSerializePIDType() As Boolean
        'See http://msdn.microsoft.com/en-us/library/53b8022e(v=vs.100).aspx
        Return PIDType.HasValue
    End Function

    <XmlElement("PID")>
    Public Property PID As String

    <XmlElement("CountryCode")>
    Public Property CountryCode As String = Nothing

    <XmlElement("RHIF")>
    Public Property RHIF As String

    <XmlElement("HealthRegion")>
    Public Property HealthRegion As String

    <XmlElement("CardNo")>
    Public Property CardNo As Integer

    <XmlElement("AmbulatoryNo")>
    Public Property AmbulatoryNo As Integer

    <XmlElement("LeadingDiagnosis")>
    Public Property LeadingDiagnosis As String

    <XmlElement("SupplementalDiagnosis")>
    Public Property SupplementalDiagnosis As String = Nothing

    <XmlElement("DOB", DataType:="date")>
    Public Property DOB As Date

    <XmlElement("Token")>
    Public Property Token As String = Nothing

    <XmlElement("ProviderID")>
    Public Property ProviderID As Integer? = Nothing

    <XmlElement("ResultsToken")>
    Public Property ResultsToken As String = Nothing

    <XmlElement("Examination")>
    Public Property Examinations As List(Of Examination) = New List(Of Examination)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    Public Property ExaminationsArray() As <MarshalAs(UnmanagedType.Struct,
                       SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Get
            Return Me.Examinations.Cast(Of Object).ToArray
        End Get
        Set(<MarshalAs(UnmanagedType.Struct,
                       SafeArraySubType:=VarEnum.VT_ARRAY)> ByVal value As Object())
            Me.Examinations.Clear()
            For Each o As Object In value
                Me.Examinations.Add(CType(o, Examination))
            Next
        End Set
    End Property

    <XmlElement("Note")>
    Public Property Note As String = Nothing

    <XmlElement("Status")>
    Public Property Status As String = Nothing

    <XmlElement("Doctor")>
    Public Property ProviderDoctor As ProviderDoctor = Nothing

    Public Function Clone() As Object Implements System.ICloneable.Clone
        Return MyBase.MemberwiseClone
        'TODO: Clone Examinations
    End Function
End Class

<XmlType(TypeName:="PracticeHolder")>
Public Class PracticeHolder

    <XmlElement("UIN")>
    Public Property UIN As String

    <XmlElement("GivenName")>
    Public Property GivenName As String

    <XmlElement("MiddleName")>
    Public Property MiddleName As String

    <XmlElement("FamilyName")>
    Public Property FamilyName As String

    <XmlElement("Speciality")>
    Public Property Speciality As String

    <XmlElement("PracticeCode")>
    Public Property PracticeCode As String

    <XmlElement("PracticeName")>
    Public Property PracticeName As String

End Class

<XmlType(TypeName:="Result")>
Public Class Result

    <XmlElement("ID")>
    Public Property ID As Integer

    <XmlElement("LoincID")>
    Public Property LoincID As String

    <XmlElement("Name")>
    Public Property Name As String

    <XmlElement("TextResult")>
    Public Property TextResult As String

    <XmlElement("DecimalResult")>
    Public Property DeciamlResult As Decimal?

    <XmlElement("Units")>
    Public Property Units As String

    <XmlElement("Range")>
    Public Property Range As String

    <XmlElement("Flag")>
    Public Property Flag As String

    <XmlElement("Status")>
    Public Property Status As String

    <XmlElement("Note")>
    Public Property Note As String

    <XmlElement("Rank")>
    Public Property Rank As Integer?

End Class

<XmlRoot("Examination")>
Public Class Examination
    Implements IComparable

    <XmlElement("Code")>
    Public Property Code As String = ""

    Public Function CompareTo(obj As Object) As Integer Implements System.IComparable.CompareTo
        If obj Is Nothing Or (Not TypeOf obj Is Examination) Then Return 0
        Return String.Compare(Me.Code, DirectCast(obj, Examination).Code)
    End Function

    <XmlElement("Result")>
    Public Property Results As List(Of Result) = New List(Of Result)

End Class

<XmlRoot("Doctor")>
Public Class ProviderDoctor

    <XmlElement("UIN")>
    Public Property UIN As String

    <XmlElement("GivenName")>
    Public Property GivenName As String

    <XmlElement("MiddleName")>
    Public Property MiddleName As String = Nothing

    <XmlElement("FamilyName")>
    Public Property FamilyName As String

End Class
