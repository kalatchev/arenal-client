﻿Imports System.Runtime.InteropServices
Imports System.Xml.Serialization

<XmlRoot("PublisherRoles")>
Public Class PublisherRoles

    <XmlElement("PublisherRole")>
    Public Property Items As List(Of PublisherRole) = New List(Of PublisherRole)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    ''' <returns>Array Of Objects</returns>
    Public Function ItemsAsArrayOfObjects() As <MarshalAs(UnmanagedType.Struct, SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Return Me.Items.Cast(Of Object).ToArray
    End Function

End Class

<XmlRoot("PublisherRole")>
Public Class PublisherRole

    <XmlElement("ID")>
    Public Property ID As Integer = 0

    <XmlElement("Name")>
    Public Property Name As String = ""

End Class
