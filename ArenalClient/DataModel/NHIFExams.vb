﻿Imports System.Runtime.InteropServices
Imports System.Xml.Serialization

<XmlRoot("NHIFPacks")>
Public Class NHIFPacks

    <XmlElement("NHIFPack")>
    Public Property Items As List(Of NHIFPack) = New List(Of NHIFPack)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    ''' <returns>Array Of Objects</returns>
    Public Function ItemsAsArrayOfObjects() As <MarshalAs(UnmanagedType.Struct, SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Return Me.Items.Cast(Of Object).ToArray
    End Function

End Class

<XmlRoot("NHIFPack")>
Public Class NHIFPack
    Implements IComparable

    <XmlElement("Code")>
    Public Property Code As String

    <XmlElement("Name")>
    Public Property Name As String = Nothing

    <XmlElement("NHIFExam")>
    Public Property NHIFExams As List(Of NHIFExam) = New List(Of NHIFExam)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    ''' <returns>Array Of Objects</returns>
    Public Function ItemsAsArrayOfObjects() As <MarshalAs(UnmanagedType.Struct, SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Return Me.NHIFExams.Cast(Of Object).ToArray
    End Function

    Public Function CompareTo(obj As Object) As Integer Implements System.IComparable.CompareTo
        If Not TypeOf obj Is NHIFPack Then Return 0
        Return String.Compare(Me.Code, CType(obj, NHIFPack).Code)
    End Function

End Class

<XmlRoot("NHIFExam")>
Public Class NHIFExam
    Implements IComparable

    <XmlElement("Code")>
    Public Property Code As String

    <XmlElement("Name")>
    Public Property Name As String = Nothing

    <XmlElement("IsSpecialized")>
    Public Property IsSpecialized As Boolean = False

    Public Function CompareTo(obj As Object) As Integer Implements System.IComparable.CompareTo
        If Not TypeOf obj Is NHIFExam Then Return 0
        Return String.Compare(Me.Code, CType(obj, NHIFExam).Code)
    End Function

End Class
