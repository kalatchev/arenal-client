﻿Imports System.Runtime.InteropServices
Imports System.Xml.Serialization

<XmlRoot("Countries")>
Public Class Countries

    <XmlElement("Country")>
    Public Property Items As List(Of Country) = New List(Of Country)

    ''' <summary>
    ''' COM Helper
    ''' </summary>
    ''' <returns>Array Of Objects</returns>
    Public Function ItemsAsArrayOfObjects() As <MarshalAs(UnmanagedType.Struct, SafeArraySubType:=VarEnum.VT_ARRAY)> Object()
        Return Me.Items.Cast(Of Object).ToArray
    End Function

End Class

<XmlRoot("Country")>
Public Class Country

    <XmlElement("Code")>
    Public Property Code As String

    <XmlElement("Name")>
    Public Property Name As String

    <XmlElement("OrgMember", IsNullable:=True)>
    Public Property OrgMember As String

End Class
