﻿Imports System.Xml.Serialization

<XmlRoot("ErrorInfo")>
Public Class ArenalError

    <XmlElement("Code")>
    Public Property ErrorCode As Integer

    <XmlElement("Description")>
    Public Property Message As String

    <XmlElement("Details")>
    Public Property Details As String

End Class
