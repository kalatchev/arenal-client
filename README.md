# Arenal Client Library
This is the official client library for [Arenal](https://awp.skyware-group.com/) server

You can use NuGet package:

`Install-Package Skyware.Arenal.ArenalClient -Version 0.7.0`
